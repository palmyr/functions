<?php

namespace Palmyr\Functions\CommonUtils\Foo;

class Foo
{

    public function foo(): void
    {
        echo 'hello world!';
    }

}